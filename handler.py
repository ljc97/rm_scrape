from .process_property import process_rm_url
import json
from datetime import datetime


class DateTimeEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()

        return json.JSONEncoder.default(self, o)


def handle(req):
    """handle a request to the function
    Args:
        req (str): request body
    """

    data = json.loads(req)

    if "url" not in data:
        return "No URL found in request {}".format(req), 400
    url = data["url"]
    workers = 16
    age = None
    if "age" in data:
        age = data["age"]

    property_data = process_rm_url(url, age, workers)

    response = json.dumps(property_data, cls=DateTimeEncoder)

    return response, 200

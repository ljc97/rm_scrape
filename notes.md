# Notes

## URL

### Examples

* https://www.rightmove.co.uk/property-to-rent/find.html?keywords=&sortType=6&viewType=LIST&channel=RENT&index=0&radius=0.0&locationIdentifier=USERDEFINEDAREA%5E%7B%22polylines%22%3A%22gttyHzi_%40%60pAc%5Etc%40%7EeEcyB%7CiCiRal%40uM_nBje%40wuC%22%7D
* https://www.rightmove.co.uk/property-to-rent/find.html?locationIdentifier=USERDEFINEDAREA%5E%7B%22polylines%22%3A%22gttyHzi_%40%60pAc%5Etc%40~eEcyB%7CiCiRal%40uM_nBje%40wuC%22%7D&maxPrice=35000&minPrice=250&propertyTypes=&mustHave=&dontShow=&furnishTypes=&keywords=
* https://www.rightmove.co.uk/property-to-rent/find.html?locationIdentifier=USERDEFINEDAREA%5E%7B%22polylines%22%3A%22gttyHzi_%40%60pAc%5Etc%40~eEcyB%7CiCiRal%40uM_nBje%40wuC%22%7D&maxBedrooms=4&minBedrooms=3&maxPrice=3000&minPrice=500&propertyTypes=&mustHave=&dontShow=student%2Cretirement%2ChouseShare&furnishTypes=&keywords=
https://www.rightmove.co.uk/property-to-rent/find.html?locationIdentifier=USERDEFINEDAREA%5E%7B%22polylines%22%3A%22gttyHzi_%40%60pAc%5Etc%40~eEcyB%7CiCiRal%40uM_nBje%40wuC%22%7D&maxBedrooms=4&minBedrooms=3&maxPrice=3000&minPrice=500&index=24&propertyTypes=&mustHave=&dontShow=student%2Cretirement%2ChouseShare&furnishTypes=&keywords=

### Analysis

* find.html vs map.html
* viewType=LIST
* channel=RENT
* index=0 - pagination, 24 per page, index=24 -> page 2
* radius=0.0
* locationIdentifier=USERDEFINEDAREA%5E%7B%22polylines%22%3A%22gttyHzi_%40%60pAc%5Etc%40%7EeEcyB%7CiCiRal%40uM_nBje%40wuC%22%7D
  * Seems to be the entire polygon packed into locationIdentifier
* minPrice/maxPrice
* dontShow
* minBedrooms/maxBedrooms

## Results Pages

* Main results container "l-propertySearch-results propertySearch-results"
* Result list in "l-searchResults"
* Each result wrapped in "l-searchResult is-list"
* Normal results "propertyCard", sponsored "propertyCard propertyCard--featured"
* Link to property in "propertyCard-link"
* Next button is "pagination-button pagination-direction pagination-direction--next"
  * Loaded via JS
* On last page 25 results still shown
  * Sponsored + actual results + padding with "is-hidden"

## Property Page

* Main Content in "primaryContent"
* RHS Content in "secondaryContent"
* Desc/Map/School tabs in "clearfix tabbed-content-nav print-hidden"
* Property description in "tabbed-content-tab clearfix active"
* Description in itemprop="description"
* Key features in "sect key-features"
* Letting info Div "lettingInformation"
* Map/broadband stuff is in "right desc-widgets"
* Broadband check is "check-broadband-speed"
  * Results go in "results-wrapper"
  * Under "top-provider"
  * Request URL: https://partnerships-broadband.comparethemarket.com/v1/broadband/rightmove/NW62JR?apikey=4cfa325d717921499989a2e3e5fd7b067530a6fa542cbec6642753bcb55a4083
  * Response: {"bundle_type":["FibreBroadbandOnly","BroadbandOnly"],"postcode":"NW62JR","speed":528384,"speed_display":"528Mb","provider_name":"Virgin Media","provider_logo_url":"https://bucket.cdndtl.co.uk/_v1/celsus/providers/logos_500px/virgin_media.v1.png","contract_length":"18 months","download_limit":"Truly Unlimited","offer_image":"https://bucket.cdndtl.co.uk/old-cdn/2/General/virgin_noline-hide.png","first_year_cost":"£587.00","monthly_cost":"£47.94","setup_cost":"£35.00","cost_breakdown":[{"key":"FirstMonthsCost","description":"(for 18 months)","value":"£46.00 p/m"}],"all_offers_url":"https://broadband.comparethemarket.com/deeplink/packages/fastest?location=NW62JR&utm_source=Partner&utm_campaign=Rightmove_Broadband&utm_medium=Website&utm_content=BroadbandWidget&AFFCLIE=EI34&src=EI34"}
  * API Request based only on postcode
  * Script contains "broadbandCtmUrl" with API url
import argparse
import concurrent.futures
import json
import re
from datetime import datetime, timedelta
from typing import AnyStr, SupportsInt

import requests
import unicodecsv as csv
from bs4 import BeautifulSoup
from bs4 import PageElement

BROADBAND_SPEED_MAP = {}
EXPECTED_FIELDS = ['property_link', 'property_header', 'price', 'added', 'address', 'furnishing', 'floorplan',
                   'broadband_speed', 'description']


def process_search_page(url: AnyStr):
    print("Processing Search {}".format(url))
    property_links = []
    page = requests.get(url)
    parsed_page = BeautifulSoup(page.content, 'html.parser')

    property_cards = parsed_page.find_all("div", class_="l-searchResult")

    for pc in property_cards:
        property_link = process_property_card(pc)
        if property_link is not None:
            property_links.append(property_link)
    return property_links


def process_property_card(property_card: PageElement):
    if property_card is None:
        return None
    if "is-hidden" in property_card["class"]:
        # Ignore hidden results from last page
        return None
    if property_card.find("div", class_="propertyCard--featured") is not None:
        # Ignore Sponsored
        return None
    img_link = property_card.find("a", class_="propertyCard-img-link")
    property_link = img_link["href"]
    return "https://www.rightmove.co.uk{}".format(property_link)


def process_property_link(property_link):
    print("Processing Property {}".format(property_link))
    page = requests.get(property_link)
    parsed_page = BeautifulSoup(page.content, 'html.parser')

    property_info = {"property_link": property_link}

    try:
        property_header = parsed_page.find("div", class_="property-header-bedroom-and-price").find("h1").text.strip()
        property_info["property_header"] = property_header
    except:
        property_info["property_header"] = None

    try:
        address = parsed_page.find("div", class_="property-header-bedroom-and-price").find("address").find("meta",
                                                                                                           itemprop="streetAddress").text.strip()
        property_info["address"] = address
    except:
        property_info["address"] = None

    try:
        furnishing = parsed_page.find("div", id="lettingInformation").find("table").find("td",
                                                                                         id="furnishedType").text.strip()
        property_info["furnishing"] = furnishing
    except:
        property_info["furnishing"] = None

    try:
        description = parsed_page.find("div", class_="description").find("p", itemprop="description").text.strip()
        description = re.sub("[\.]{0,1}(\n\r|\r\n|\n|\r)", ". ", description)
        property_info["description"] = description
    except:
        property_info["description"] = None

    try:
        price = parsed_page.find("p", id="propertyHeaderPrice").find("strong").text.strip()
        price = price.replace("£", "")
        price = price.replace(",", "")
        if "pw" in price:
            price = price.replace("pw", "")
            price = round(int(price) * 52 / 12, 2)
        elif "pcm" in price:
            price = price.replace("pcm", "")
            price = round(int(price), 2)
        property_info["price"] = price
    except:
        property_info["price"] = None

    try:
        # find broadband api call url
        full_text = parsed_page.prettify()
        broadband_url_start = full_text.find("broadbandCtmUrl")
        broadband_url_end = full_text[broadband_url_start:].find("\n") + broadband_url_start
        broadband_url_line = full_text[broadband_url_start:broadband_url_end].strip()
        start_index = broadband_url_line.find('https://')
        broadband_url = broadband_url_line[start_index:-1]

        if broadband_url in BROADBAND_SPEED_MAP:
            property_info["broadband_speed"] = BROADBAND_SPEED_MAP[broadband_url]
        else:
            # make broadband api call
            broadband_response = requests.get(broadband_url)
            broadband_data = json.loads(broadband_response.content)
            broadband_speed = broadband_data["speed_display"]
            property_info["broadband_speed"] = broadband_speed
            BROADBAND_SPEED_MAP[broadband_url] = broadband_speed
    except:
        property_info["broadband_speed"] = None

    try:
        # find property age
        full_text = parsed_page.prettify()
        age_start = full_text.find("'property'")
        age_end = full_text[age_start:].find("}));")
        age_text = full_text[age_start: age_start + age_end + 1]
        age_start = age_text.find("{\"location\"")
        age_text = age_text[age_start:]

        age_data = json.loads(age_text)  # lots of data in this dict
        if "propertyInfo" in age_data:
            if "added" in age_data["propertyInfo"]:
                property_info["added"] = datetime.strptime(age_data["propertyInfo"]["added"], "%Y%m%d")
            if "furnishedType" in age_data["propertyInfo"]:
                if property_info["furnishing"] is None and age_data["propertyInfo"]["furnishedType"] is not None:
                    property_info["furnishing"] = age_data["propertyInfo"]["furnishedType"]
        if "location" in age_data:
            if "postcode" in age_data["location"]:
                property_info["postcode"] = age_data["location"]["postcode"]
    except:
        if not "added" in property_info:
            property_info["added"] = None

    if parsed_page.find("li", id="floorplansTab") is not None:
        property_info["floorplan"] = "true"
    else:
        property_info["floorplan"] = "false"

    found_value = False
    for key in EXPECTED_FIELDS:
        if key != "property_link" and key in property_info:
            found_value = True

    if not found_value:
        print("Unable to find any value(s) for property {}", property_link)

    return property_info


def parse_url(url: AnyStr):
    if url.startswith("https://www.rightmove.co.uk/property-to-rent/map.html?"):
        # on map page, need list
        url = url.replace("https://www.rightmove.co.uk/property-to-rent/map.html",
                          "https://www.rightmove.co.uk/property-to-rent/find.html")
    if "index=" in url:
        # have index, need to remove
        url = re.sub("[&]{0,1}index=[0-9A-z{}]*[&]{0,1}", "&", url)
        if "?&" in url:
            url.replace("?&", "?")
        if url[-1] == "&":
            url = url[:-1]
        if "&&" in url:
            url.replace("&&", "&")
    # add index with format brackets
    url = url + "&index={}"

    return url


def process_rm_url(url: AnyStr, age: SupportsInt = None, workers: SupportsInt = 1):
    url_template = parse_url(url)
    if url_template is None:
        print("Error parsing URL")
        exit(-1)

    page = requests.get(url.format(0))

    parsed_page = BeautifulSoup(page.content, 'html.parser')

    total_properties_span = parsed_page.find("span", class_="searchHeader-resultCount")
    total_properties = None
    try:
        total_properties = int(total_properties_span.text)
    except:
        print("Unable to determine total number of properties, exiting")
        exit(-1)

    print("Found {} properties".format(total_properties))

    number_of_pages = int((total_properties / 24) + 1)

    print("Examining {} pages".format(number_of_pages))

    executor = concurrent.futures.ThreadPoolExecutor(max_workers=workers)

    property_links = []
    property_links_futures = []
    for page_num in range(0, number_of_pages):
        page_index = page_num * 24
        target_url = url_template.format(page_index)
        property_links_futures.append(executor.submit(process_search_page, target_url))

    for plf in concurrent.futures.as_completed(property_links_futures):
        property_links.extend(plf.result())

    property_data = []
    property_futures = []

    for pl in property_links:
        property_futures.append(executor.submit(process_property_link, pl))

    for plf in concurrent.futures.as_completed(property_futures):
        property_data.append(plf.result())

    threshold_date = datetime.now() - timedelta(days=age)

    property_data = list(filter(lambda p: (p["added"] is None) or (p["added"] >= threshold_date), property_data))

    return property_data


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Retrieve property details from rightmove')
    parser.add_argument("url", help='URL to Scrape')
    parser.add_argument("--age", action="store", required=False, type=int, help="Max Age of included Adverts")
    parser.add_argument("--workers", action="store", required=False, type=int, help="Number of parallel workers")
    args = parser.parse_args()

    if args.workers is not None:
        NUM_WORKERS = args.workers

    if args.age is not None:
        MAX_AGE = args.age

    property_data = process_rm_url(args.url, args.age, args.workers)

    print("Exporting Data")
    with open('data.csv', 'wb') as csvfile:
        fieldnames = EXPECTED_FIELDS
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames, extrasaction='ignore')

        writer.writeheader()
        writer.writerows(property_data)
